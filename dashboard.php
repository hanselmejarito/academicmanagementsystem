<!DOCTYPE html>
<html>
    <head>
	    <link rel="stylesheet" type="text/css" href="css/materialize.min.css">
	    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    	<link rel="stylesheet" href="css/jquery.dataTables.min.css">
	    <link rel="stylesheet" type="text/css" href="css/style.css">
  		<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" />

	  	<title>AMS</title>
 	</head>
<body class="grey lighten-4 ">
	<div class="modal" id="logoutForm" role="dialog">
	    <div class="modal-content">
	 		<h5>Ready to Leave?</h5>
	 		<p>Select "Yes" below if you are ready to end your current session.</p>
	    </div>
	    <div class="modal-footer">
	    	<a class="modal-action modal-close waves-effect btn-flat">No</a>
	    	<a href="#!" class="modal-action modal-close waves-effect btn-flat confirmBtnColor white-text" id="confirmLogout">Yes</a>
	    </div>
	</div>
	<header>
		<nav class="grey lighten-4 z-depth-0">
			<div class="nav-wrapper grey lighten-4">
	            <ul class="AdminOnline">
	              	<b>
	              		<li><span class="grey-text "><i class="fa fa-circle green-text onlineStyle" aria-hidden="true"></i>Online</span></li>
	              	</b>
	            </ul>
	            <ul class="right">
	              	<b>
	              		<li>
				          	<a href="#logoutForm" class="modal-trigger grey-text waves-effect">
				            <i class="fa fa-fw fa-sign-out"></i>Logout</a>
				        </li>
	              	</b>
	            </ul>
	        </div>
		  	<ul id="slide-out" class="side-nav fixed z-depth-0 sideNavColor">
  			    <li>
			    	<div class="user-view center">
						<div class="background white adminBgStyle"></div>
						<a href="#!user"><img class="circle align-center" src="img/marco.png" style="margin: auto;"></a>
						<a href="#!administrator"><span class="grey-text name">ADMINISTRATOR</span></a>
				    </div>
				</li>
			    <li>
			    	<a href="#!" class="white-text waves-effect Style">Admission</a>
			    </li>
			    <li>
			    	<a href="#!" class="white-text waves-effect">Projects</a>
			    </li>
			    <ul class="collapsible collapsible-accordion">
				    <li>
				    	<a href="#!" class="white-text collapsible-header waves-effect ">Accounting<i class="fa fa-caret-down white-text right" aria-hidden="true"></i></a>
				    	<div class="collapsible-body center sideNavColor">
				          	<ul>
				            	<li><a href="#!" class="white-text">Transact</a></li>
				            	<li><a href="#!" class="white-text">Particular</a></li>
				            	<li><a href="#!" class="white-text">Expense</a></li>
				            	<li><a href="#!" class="white-text">Deposits</a></li>
				            	<li><a href="#!" class="white-text">Logs</a></li>
				            	<li><a href="#!" class="white-text">Reports</a></li>
				            </ul>
				        </div>
				    </li>
				</ul>
				<li>
			    	<a href="#!" class="white-text waves-effect">Assessment List</a>
			    </li>
			    <li>
			    	<a href="#!" class="white-text waves-effect">Semester Management</a>
			    </li>
				<li>
			    	<a href="#!" class="white-text waves-effect">Instructor Management</a>
			    </li>
				<li>
			    	<a href="#!" class="white-text waves-effect">Course Management</a>
			    </li>
			    <li>
			    	<a href="#!" class="white-text waves-effect">Curriculum Management</a>
			    </li>
				<li>
			    	<a href="#!" class="white-text waves-effect">Grade Management</a>
			    </li>
			    <li>
			    	<a href="#!" class="white-text waves-effect">Enrollment</a>
			    </li>
			</ul>
		  	<a href="#" data-activates="slide-out" class="button-collapse"><i class="fa fa-bars grey-text" aria-hidden="true"></i>
		</nav>
	</header>
<div class="main">
	<div class="container-fluid">
		<div class="row dashboard">
	    	<div class="col l12 s12">
			    <a href="dashboard.php" class="">Dashboard</a>
			    <a class="grey-text"><i class="fa fa-chevron-right" aria-hidden="true"></i>Admission</a>
			</div>
		</div>
		<div class="col l12 s12">
			<div class="row">
		      	<div class="col l3">
		        	<div class="card-panel fstmrgnTop white z-depth-0">
		          		<h1 class="green-text center" id=""><b>4300</b></h1>
			          	<b>
			          		<span class="grey-text">TOTAL NO. OF STUDENTS</span>
			          	</b>
		        	</div>
		      	</div>
		      	<div class="col l3">
		        	<div class="card-panel mrgnTop white z-depth-0">
		        		  	<p class="col l12">
				                <b><select class="browser-default grey-text" id="monthV">
				                    <option value="" disabled="" selected="" class="white">Month</option>
				                    <option value="1" id="JanEnd" class=" white waves-effect">January</option>
				                    <option value="2" class=" white waves-effect">February</option>
				                    <option value="3" class=" white waves-effect">March</option>
				                    <option value="4" class=" white waves-effect">April</option>
				                    <option value="5" class=" white waves-effect">May</option>
				                    <option value="6" class=" white waves-effect">June</option>
				                    <option value="7" class=" white waves-effect">July</option>
				                    <option value="8" class=" white waves-effect">August</option>
				                    <option value="9" class=" white waves-effect">September</option>
				                    <option value="10" class=" white waves-effect">October</option>
				                    <option value="11" class=" white waves-effect">November</option>
				                    <option value="12" class=" white waves-effect">December</option>
				                </select></b>
				               	<span class="redErrorAlert hide" id="monthVError">Required*</span>
				            </p>
		          		<h1 class="red-text center" id=""><b>40%</b></h1>
			          	<b>
			          		<span class="grey-text">STUDENT Dropped</span>
			          	</b>
		        	</div>
		      	</div>
		      	<div class="col l6">
	        		<div class="card-panel white z-depth-0">
		          		<p class="grey-text" id="">New Students / 2015</p>
			          	<div class="row">
			          		<div class="col s3">
			          			<b>
			          				<span class="grey-text">BSIT</span>
			          			</b>
			          		</div>
			          		<div class="col s5">
			          			<b>
			          				<span class="grey-text">Drake,Joshua R.</span>
			          			</b>
			          		</div>
			          		<div class="col s4">
			          			<b>
			          				<span class="grey-text">New</span>
			          			</b>
			          		</div>
			          	</div>
			          	<div class="row">
			          		<div class="col s3">
			          			<b>
			          				<span class="grey-text">BSIT</span>
			          			</b>
			          		</div>
			          		<div class="col s5">
			          			<b>
			          				<span class="grey-text">Drake,Joshua R.</span>
			          			</b>
			          		</div>
			          		<div class="col s4">
			          			<b>
			          				<span class="grey-text">New</span>
			          			</b>
			          		</div>
			          	</div>
			          	<a href="#!" class="right sad">View more</a>
		        	</div>
		      	</div>
		    </div>
		</div>
		<div class="row">
			<div class="col s12 z-depth-1 rowStyle">
				<div class="row">
					<div class="col s10">
						<h5 class="grey-text ">Student Information</h5>
					</div>
					<div class="modal modal-fixed-footer" id="addStudent" role="dialog">
					    <div class="modal-content">
			         		<h5><p class="grey-text">ADD</p></h5>
			         		<div class="row">
						        <form class="col s12">
						          	<div class="row modal-form-row">
						            	<div class="input-field col s12">
						              		<input id="firstName" type="text" class="validate vldtMargin">
						              		<label for="firstName">First Name</label>
						              		<span class="redErrorAlert" id="firstNameError">required*</span>
						            	</div>
						          	</div>
						          	<div class="row">
						            		<div class="input-field col s12">
						              		<input id="middleInitial" type="text" class="validate vldtMargin">
						              		<label for="middleInitial">Middle Initial</label>
			                		  		<span class="redErrorAlert" id="middleInitialError">required*</span>
						            	</div>
						          	</div> 
						          	<div class="row">
						            	<div class="input-field col s12">
						              		<input id="lastName" type="password" class="validate vldtMargin">
						              		<label for="lastName">Last Name</label>
			                		  		<span class="redErrorAlert" id="lastNameError">required*</span>
						            	</div>
						          	</div> 
						          	<div class="row">
							            	<div class="input-field col s12">
								              	<input id="courseStdnt" type="password" class="validate vldtMargin">
								              	<label for="courseStdnt">Course</label>
					                		  	<span class="redErrorAlert" id="courseStdntError">required*</span>
							            	</div>
							           	</div>               
						        </form>
						    </div>
			            </div>
					    <div class="modal-footer">
					    	<a class="modal-action modal-close waves-effect btn-flat  ">No</a>
					    	<a href="#!" id="confirmAddStdnt" class="modal-action modal-close waves-effect confirmBtnColor btn-flat  white-text">Yes</a>
					    </div>
					</div>
					<div class="col s2">
						<a class="btn-floating btn-flat green right modal-trigger" href="#addStudent">
		                    <i class="fa fa-plus waves-effect small" aria-hidden="true"></i>
		                </a>	
					</div>
				</div>
				<div class="modal modal-fixed-footer" id="viewStdntInfo" role="dialog">
				    <div class="modal-content">
		         		<h5><p class="grey-text">DETAILS</p></h5>
		         		<div class="row">
					        <form class="col s12">
					          	<div class="row modal-form-row">
					            	<div class="input-field col s12">
					              		<input id="viewfirstName" type="text" class="validate vldtMargin">
					              		<label for="viewfirstName">First Name</label>
					              		<span class="redErrorAlert" id="viewfirstNameError">required*</span>
					            	</div>
					          	</div>
					          	<div class="row">
					            		<div class="input-field col s12">
					              		<input id="viewmiddleInitial" type="text" class="validate vldtMargin">
					              		<label for="viewmiddleInitial">Middle Initial</label>
		                		  		<span class="redErrorAlert" id="viewmiddleInitialError">required*</span>
					            	</div>
					          	</div> 
					          	<div class="row">
					            	<div class="input-field col s12">
					              		<input id="viewlastName" type="password" class="validate vldtMargin">
					              		<label for="viewlastName">Last Name</label>
		                		  		<span class="redErrorAlert" id="viewlastNameError">required*</span>
					            	</div>
					          	</div> 
					          	<div class="row">
					            	<div class="input-field col s12">
						              	<input id="viewcourseStdnt" type="password" class="validate vldtMargin">
						              	<label for="viewcourseStdnt">Course</label>
			                		  	<span class="redErrorAlert" id="viewcourseStdntError">required*</span>
					            	</div>
					           	</div>               
					        </form>
					    </div>
		            </div>
				    <div class="modal-footer">
				    	<a class="modal-action modal-close waves-effect btn-flat  ">Close</a>
				    	<!-- <a href="#!" class="modal-action modal-close waves-effect btn-flat  white-text" style="background-color: #4c8951;">Close</a> -->
				    </div>
				</div>
				<div class="modal modal-fixed-footer" id="editStdntInfo" role="dialog">
				    <div class="modal-content">
		         		<h5><p class="grey-text">EDIT</p></h5>
		         		<div class="row">
					        <form class="col s12">
					          	<div class="row modal-form-row">
					            	<div class="input-field col s12">
					              		<input id="editfirstName" type="text" class="validate vldtMargin">
					              		<label for="editfirstName">First Name</label>
					              		<span class="redErrorAlert" id="editfirstNameError">required*</span>
					            	</div>
					          	</div>
					          	<div class="row">
					            		<div class="input-field col s12">
					              		<input id="editmiddleInitial" type="text" class="validate vldtMargin">
					              		<label for="editmiddleInitial">Middle Initial</label>
		                		  		<span class="redErrorAlert" id="editmiddleInitialError">required*</span>
					            	</div>
					          	</div> 
					          	<div class="row">
					            	<div class="input-field col s12">
					              		<input id="editlastName" type="password" class="validate vldtMargin">
					              		<label for="editlastName">Last Name</label>
		                		  		<span class="redErrorAlert" id="editlastNameError">required*</span>
					            	</div>
					          	</div> 
					          	<div class="row">
						            	<div class="input-field col s12">
							              	<input id="editcourseStdnt" type="password" class="validate vldtMargin">
							              	<label for="editcourseStdnt">Course</label>
				                		  	<span class="redErrorAlert" id="editcourseStdntError">required*</span>
						            	</div>
						           	</div>               
					        </form>
					    </div>
		            </div>
				    <div class="modal-footer">
				    	<a class="modal-action modal-close waves-effect btn-flat  ">No</a>
				    	<a href="#!" id="confirmEdit" class="modal-action modal-close confirmBtnColor waves-effect btn-flat  white-text">Yes</a>
				    </div>
				</div>
		        <table id="usertable" class="display" cellspacing="0" width="100%">
			        <thead class="grey-text">
			            <tr>
			                <th>ID</th>
			                <th>First Name</th>
			                <th>Middle Initial</th>
			                <th>Last Name </th>
			                <th>Course</th>
			                <th>Details</th>
			                <th>Edit</th>
			            </tr>
			        </thead>
			        <tfoot class="grey-text">
			            <tr>
			                <th>ID</th>
			                <th>First Name</th>
			                <th>Middle Initial</th>
			                <th>Last Name </th>
			                <th>Course</th>
			                <th>Details</th>
			                <th>Edit</th>
			            </tr>
			        </tfoot>
			        <tbody class="">
			            <tr>
			                <td>sad Nixon</td>
			                <td>System Architect</td>
			                <td>Tiger Nixon</td>
			                <td>Tiger Nixon</td>
			                <td>1</td>
			                <td><a href="#viewStdntInfo" class="btn-flat modal-trigger white-text waves-effect" style="border-radius: 20px; background-color: #4c8951;">View</a></td>
			                <td><a href="#editStdntInfo" class="modal-trigger"><i class="fa fa-pencil-square-o small waves-effect" aria-hidden="true"></i></a></td>
			            </tr>
			            <tr>
			                <td>sad Nixon</td>
			                <td>System Architect</td>
			                <td>Tiger Nixon</td>
			                <td>Tiger Nixon</td>
			                <td>1</td>
			                <td><a href="#viewStdntInfo" class="btn-flat modal-trigger white-text waves-effect" style="border-radius: 20px; background-color: #4c8951;">View</a></td>
			                <td><a href="#editStdntInfo" class="modal-trigger"><i class="fa fa-pencil-square-o small waves-effect" aria-hidden="true"></i></a></td>
			            </tr>
			            <tr>
			                <td>sad Nixon</td>
			                <td>System Architect</td>
			                <td>Tiger Nixon</td>
			                <td>Tiger Nixon</td>
			                <td>1</td>
			                <td><a href="#viewStdntInfo" class="btn-flat modal-trigger white-text waves-effect" style="border-radius: 20px; background-color: #4c8951;">View</a></td>
			                <td><a href="#editStdntInfo" class="modal-trigger"><i class="fa fa-pencil-square-o small waves-effect" aria-hidden="true"></i></a></td>
			            </tr>
			            <tr>
			                <td>sad Nixon</td>
			                <td>System Architect</td>
			                <td>Tiger Nixon</td>
			                <td>Tiger Nixon</td>
			                <td>1</td>
			                <td><a href="#viewStdntInfo" class="btn-flat modal-trigger white-text waves-effect" style="border-radius: 20px; background-color: #4c8951;">View</a></td>
			                <td><a href="#editStdntInfo" class="modal-trigger"><i class="fa fa-pencil-square-o small waves-effect" aria-hidden="true"></i></a></td>
			            </tr>
			            <tr>
			                <td>sad Nixon</td>
			                <td>System Architect</td>
			                <td>Tiger Nixon</td>
			                <td>Tiger Nixon</td>
			                <td>1</td>
			                <td><a href="#viewStdntInfo" class="btn-flat modal-trigger white-text waves-effect" style="border-radius: 20px; background-color: #4c8951;">View</a></td>
			                <td><a href="#editStdntInfo" class="modal-trigger"><i class="fa fa-pencil-square-o small waves-effect" aria-hidden="true"></i></a></td>
			            </tr>
			            <tr>
			                <td>sad Nixon</td>
			                <td>System Architect</td>
			                <td>Tiger Nixon</td>
			                <td>Tiger Nixon</td>
			                <td>1</td>
			                <td><a href="#viewStdntInfo" class="btn-flat modal-trigger white-text waves-effect" style="border-radius: 20px; background-color: #4c8951;">View</a></td>
			                <td><a href="#editStdntInfo" class="modal-trigger"><i class="fa fa-pencil-square-o small waves-effect" aria-hidden="true"></i></a></td>
			            </tr>
			            <tr>
			                <td>sad Nixon</td>
			                <td>System Architect</td>
			                <td>Tiger Nixon</td>
			                <td>Tiger Nixon</td>
			                <td>1</td>
			                <td><a href="#viewStdntInfo" class="btn-flat modal-trigger white-text waves-effect" style="border-radius: 20px; background-color: #4c8951;">View</a></td>
			                <td><a href="#editStdntInfo" class="modal-trigger"><i class="fa fa-pencil-square-o small waves-effect" aria-hidden="true"></i></a></td>
			            </tr>
			            <tr>
			                <td>sad Nixon</td>
			                <td>System Architect</td>
			                <td>Tiger Nixon</td>
			                <td>Tiger Nixon</td>
			                <td>1</td>
			                <td><a href="#viewStdntInfo" class="btn-flat modal-trigger white-text waves-effect" style="border-radius: 20px; background-color: #4c8951;">View</a></td>
			                <td><a href="#editStdntInfo" class="modal-trigger"><i class="fa fa-pencil-square-o small waves-effect" aria-hidden="true"></i></a></td>
			            </tr>
			            <tr>
			                <td>sad Nixon</td>
			                <td>System Architect</td>
			                <td>Tiger Nixon</td>
			                <td>Tiger Nixon</td>
			                <td>1</td>
			                <td><a href="#viewStdntInfo" class="btn-flat modal-trigger white-text waves-effect" style="border-radius: 20px; background-color: #4c8951;">View</a></td>
			                <td><a href="#editStdntInfo" class="modal-trigger"><i class="fa fa-pencil-square-o small waves-effect" aria-hidden="true"></i></a></td>
			            </tr>
			            <tr>
			                <td>sad Nixon</td>
			                <td>System Architect</td>
			                <td>Tiger Nixon</td>
			                <td>Tiger Nixon</td>
			                <td>1</td>
			                <td><a href="#viewStdntInfo" class="btn-flat modal-trigger white-text waves-effect" style="border-radius: 20px; background-color: #4c8951;">View</a></td>
			                <td><a href="#editStdntInfo" class="modal-trigger"><i class="fa fa-pencil-square-o small waves-effect" aria-hidden="true"></i></a></td>
			            </tr>
			            <tr>
			                <td>sad Nixon</td>
			                <td>System Architect</td>
			                <td>Tiger Nixon</td>
			                <td>Tiger Nixon</td>
			                <td>1</td>
			                <td><a href="#viewStdntInfo" class="btn-flat modal-trigger white-text waves-effect" style="border-radius: 20px; background-color: #4c8951;">View</a></td>
			                <td><a href="#editStdntInfo" class="modal-trigger"><i class="fa fa-pencil-square-o small waves-effect" aria-hidden="true"></i></a></td>
			            </tr>
			        </tbody>
			    </table>
	        </div>
	    </div>
	</div>
</div>
  




  <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="js/materialize.min.js"></script>
  <script src="js/jquery.dataTables.min.js"></script>
  <!-- <script type="text/javascript" src="js/wow.min.js"></script> -->


 	<script type="text/javascript">
  		// $('.modal').modal();
    	$('.button-collapse').sideNav({
      		menuWidth: 240,
   		});
   		$('select').material_select();
   		$('.modal').modal();
   		$('#usertable').DataTable({
            select: true,
            "scrollY": 400,
            "scrollX": true,
            "lengthMenu": [10,20,30,40,50,60,70,80,90,100,]

	    });
	    $('.dataTables_length').addClass('hide');
    	$('.collapsible').collapsible();
  	</script>
 </body>
 </html>