<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <link rel="stylesheet" type="text/css" href="css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="css/animate.min.css">

  <title>AMS Login</title>
</head>

<body class="grey lighten-4">
<div class="container">
  <div class="section"></div>
  <div class="section"></div>
    <div class="row z-depth-3 white LoginForm wow bounce">
      <h5 class="brand-logo center" style="color: #4c8951;">Login Form</h5>
      <div class="divider"></div>
      <form class="col s12" >
        <div class="card-content  green white-text lighten-1" id="login_success"><i class="fa fa-check " aria-hidden="true"></i><b> Success!</b> Login Success.<img src="img/loading.gif" width="20px" height="20px"></div>
        <div class="card-content  red white-text text-darken-1" id="login_error"><i class="fa fa-times" aria-hidden="true"></i><b> Failed!</b> Failed to login.</div>
        <div class="card-content  red white-text text-darken-1" id="login_invalid"><i class="fa fa-times" aria-hidden="true"></i><b> Invalid!</b> Invalid user or pass.</div><br/>
        <div class="row">
          <div class="input-field col s11">
           <i class="fa fa-user-circle prefix  " aria-hidden="true"></i>
            <input class="validate" id="emailtxt" type="text" >
            <label for='text'>Enter your username</label>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s11">
            <i class="fa fa-lock prefix" aria-hidden="true"></i>
            <input class="validate" id="passwordtxt" type="password">
            <label for="password">Enter your password</label>
          </div>
        </div>
      </form>
      <div class='col s12'>
              <a class='col s12 btn btn-flat white-text waves-effect' id="btnSubmitLogin" style="background-color: #4c8951; border-radius: 20px;">Login</a>
      </div>
    </div>
</div>





  <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
  <script type="text/javascript" src="js/materialize.min.js"></script>
  <script type="text/javascript" src="js/wow.min.js"></script>

  <script type="text/javascript">
    new WOW().init();
            $('.button-collapse').sideNav({
          menuWidth:250,
          edge: 'left'
        });

  </script>

 </body>
 </html>